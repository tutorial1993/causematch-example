import React from 'react';
import logo from './logo.svg';
import { Counter } from './features/counter/Counter';
import './App.css';
import { Card, DonorForm, CardGrid, Search } from './components/index'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap'

function App() {
  return (
    <Container className="app mt-2">
      <DonorForm />
      <Search />
      <CardGrid />
    </Container>
  );
}

export default App;
