import actionTypes from '../actionTypes';

const initialState = {
  cards: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.ADD_CARD: {
      const cards = [...state.cards];
      cards.push(action.card);
      return {
        cards,
      }
    }

    default:
      return state;
  }
}