import { combineReducers } from 'redux';
import cards from './cards';
import inputs from './inputs';

export default combineReducers({
  cards,
  inputs,
})
