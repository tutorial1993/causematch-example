import actionTypes from '../actionTypes';

const initialState = {
  id: -1,
  name: 'Baruch Cohen',
  amount: '100',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_INPUT_NAME: {
      const { name } = action;
      return {
        ...state,
        name,
      }
    }
    case actionTypes.SET_INPUT_AMOUNT: {
      const { amount } = action;
      return {
        ...state,
        amount,
      }
    }
    case actionTypes.SET_INPUT_ID: {
        const { id } = action;
        return {
          ...state,
          id,
        }
      }
    case actionTypes.RESET_INPUT: {
        return initialState;
    }
    default:
      return state;
  }
}