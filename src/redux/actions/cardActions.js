import actionTypes from '../actionTypes';

export default {
  addCard: (card)=> ({
    type: actionTypes.ADD_CARD,
    card
  })
}