import actionTypes from '../actionTypes';

export default {
  setInputName: name => ({
    type: actionTypes.SET_INPUT_NAME,
    name,
  }),
  setInputAmount: amount => ({
    type: actionTypes.SET_INPUT_AMOUNT,
    amount
  }),
  setInputId: id => ({
    type: actionTypes.SET_INPUT_ID,
    id,
  }),
  resetInputs: () => ({
    type: actionTypes.RESET_INPUT,
  })
}