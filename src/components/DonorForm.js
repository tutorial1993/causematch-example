import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cardActions from '../redux/actions/cardActions';
import inputActions from '../redux/actions/inputActions';


export const DonorForm = () => {
    const id = useSelector(state => state.inputs.id);
    const name = useSelector(state => state.inputs.name);
    const amount = useSelector(state => state.inputs.amount);
    const dispatch = useDispatch();
  
    const addCard = () => {
      if(name && amount) {
        dispatch(cardActions.addCard({
          name,
          amount
        }))
        dispatch(inputActions.resetInputs())
      }
      console.log(name)
      console.log(amount)
    }    
    return (
        <div className="donor-form">
        <input
          type="text"
          placeholder="Name"
          name='name'
          value={name}
          onChange={e => 
            dispatch(inputActions.setInputName(e.target.value))
          }
        />
        <input
          placeholder="Amount"
          name='amount'
          type='text'
          value={amount}
          onChange={e => 
            dispatch(inputActions.setInputAmount(e.target.value))
          }
        ></input>
          <button
            onClick={addCard}
          >
            Donate now!
          </button>      
      </div>
    )
}
