export const Card = ({ name, amount }) =>{
    
    return(
        <div className='card'>
            <div className='donation-amount'>$ {amount}</div>
            <div className='card-name'>{name}</div>
            <div className='bottom-pane'></div>
        </div>
    )
}