import { Form, Button, FormControl, Navbar, NavDropdown, Nav } from 'react-bootstrap'
export const Search = () => {
    return (
        <Navbar bg="light" expand="lg">
                <Nav className="mr-auto">
                    <NavDropdown title="Sort by:" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">Recent</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Other</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-success">Search</Button>
                </Form>
        </Navbar>
    )
}