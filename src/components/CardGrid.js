import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Card } from './index';
import inputActions from '../redux/actions/inputActions';

export const CardGrid = () => {
  const dispatch = useDispatch();
  const cards = useSelector(state => state.cards.cards)

  const onItemClicked = (item, index) => {
    dispatch(inputActions.setInputId(index));
    dispatch(inputActions.setInputTitle(item.name));
    dispatch(inputActions.setInputContent(item.amount));
  }

  if(cards.length === 0) {
    return (
      <div>
        <p>There is no donation yet. Please add one.</p>
      </div>  
    )
  }

  const recentArr = cards.reverse()


  return (
    <div className='grid-container'>
      <div>
      {recentArr.map((item, index) => {
        if(item) {
          return (
            <Card
              name={item.name}
              className='card'
              amount={item.amount}
              key={item.index}
              onItemClicked={() => {
                onItemClicked(item, index);
              }}
            />      
          )
        }
        return null;
      })}
      </div>
    </div>
  );
};
